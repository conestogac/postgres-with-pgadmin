# Postgres with pgAdmin4

The point of this project is to provide everyone with a way to quickly get a Postgres Db up and running along with pgAdmin4.

## Prerequisites.

To use this project, you must have Docker installed.

To check if you have docker installed, run the following:

```bash
docker
```

If you see a list of docker commands, you are good to go.

For details on this, please visit the [official docker documentation](https://docs.docker.com/engine/install/).

The instructions will be using unix commands. If you are on Windows and not using WSL, you will need use the appropriate Powershell commands.

## How to use this repo

First thing you will need to do is clone this project to your local.

I highly suggest using git for this but downloading the project as a zip will work as well.

Next, change directory into the project. If you've used git, it would be a directory called `postgres-with-pgadmin`.

This command will get you there (note, use complete file path if you are having issues):

```bash
cd postgres-with-pgadmin
```

From here, use the following command to bring up the service:

```bash
docker-compose up -d --build
```

Lastly, you can visit pgAdmin4 at your [localhost](http://localhost/)

When you are done, be sure to shut down the services with:

```bash
docker-compose down
```

## Logging into pgAdmin4

The default username and password are:

username: postgres@example.com
password: password

When you are adding a service, the hostname you want to use is `db`.

## Common issues:

_Windows_

If you are running into permission issues, try starting the Docker engine in Admin mode.

_Linux_

If you are getting an error asking if you've started the Docker engine, then use the following to start it up:

```bash
systemctl start docker
```
